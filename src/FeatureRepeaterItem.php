<?php
namespace Dudley\Patterns\Pattern\FeatureRepeater;

use Dudley\Patterns\Abstracts\AbstractItem;
use Dudley\Patterns\Traits\HeadingTrait;
use Dudley\Patterns\Traits\ImageTrait;

/**
 * Class FeatureRepeaterItem
 *
 * @package Dudley\Patterns\Pattern\FeatureRepeater
 */
class FeatureRepeaterItem extends AbstractItem {
	use HeadingTrait;
	use ImageTrait;

	/**
	 * Text content of the FeatureRepeaterItem.
	 *
	 * @var string
	 */
	private $content;

	/**
	 * ContentRepeaterItem constructor.
	 */
	public function __construct( $heading, $content, $image ) {
		$this->heading  = $heading;
		$this->content  = $content;
		$this->img      = $image;
		$this->img_size = $this->set_img_size( 'large', FeatureRepeater::$action_name );
	}

	/**
	 * Print the content.
	 */
	public function content() {
		echo wp_kses_post( $this->content );
	}

	/**
	 * A simple array of data values this item requires to render itself.
	 *
	 * @return array
	 */
	public function requirements() {
		return [
			$this->content,
			$this->img,
		];
	}
}
