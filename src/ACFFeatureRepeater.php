<?php
namespace Dudley\Patterns\Pattern\FeatureRepeater;

/**
 * Class ACFFeatureRepeater
 *
 * @package Dudley\Patterns\Pattern\FeatureRepeater
 */
class ACFFeatureRepeater extends FeatureRepeater {
	/**
	 * @var string
	 */
	public static $meta_type = 'acf';

	/**
	 * ACFFeatureRepeater constructor.
	 */
	public function __construct() {
		if ( ! get_field( 'feature_repeater_items' ) ) {
			return;
		}

		$this->heading = get_field( 'feature_repeater_heading' );

		while ( has_sub_field( 'feature_repeater_items' ) ) {
			$this->add_item( new FeatureRepeaterItem(
				get_sub_field( 'feature_repeater_item_heading' ),
				get_sub_field( 'feature_repeater_item_content' ),
				get_sub_field( 'feature_repeater_item_image' )
			) );
		}

		parent::__construct( $this->heading, $this->items );
	}
}
