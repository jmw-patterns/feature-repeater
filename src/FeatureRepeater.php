<?php
namespace Dudley\Patterns\Pattern\FeatureRepeater;

use Dudley\Patterns\Abstracts\AbstractRepeater;
use Dudley\Patterns\Traits\HeadingTrait;

/**
 * Class FeatureRepeater
 *
 * @package Dudley\Patterns\Pattern\FeatureRepeater
 */
class FeatureRepeater extends AbstractRepeater {
	use HeadingTrait;

	/**
	 * @var string
	 */
	public static $action_name = 'feature_repeater';

	/**
	 * FeatureRepeater constructor.
	 *
	 * @param $heading
	 * @param array $items
	 */
	public function __construct( $heading, array $items ) {
		$this->heading = $heading;
		$this->items = $items;
	}

	/**
	 * A simple array of data values this item requires to render itself.
	 *
	 * @return array
	 */
	public function requirements() {
		return [
			$this->items,
		];
	}
}
