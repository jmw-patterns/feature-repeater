<?php
/**
 * @var $module \Dudley\Patterns\Pattern\FeatureRepeater\FeatureRepeater
 * @var $item   \Dudley\Patterns\Pattern\FeatureRepeater\FeatureRepeaterItem
 */
?>

<section class="feature-repeater">
	<?php if ( $module->get_heading() ) : ?>
		<h2 class="feature-repeater__hd"><?php $module->heading(); ?></h2>
	<?php endif; ?>

	<div class="feature-repeater__items">
		<?php foreach ( $module->get_items() as $item ) : ?>
			<div class="feature-repeater__item feature-repeater-item">
				<div class="feature-repeater-item__inner">
					<div class="feature-repeater-item__img">
						<img src="<?php $item->img_url(); ?>" alt="<?php $item->img_alt(); ?>"
							 srcset="<?php $item->img_srcset( 'large' ); ?>" sizes="<?php $item->img_sizes(); ?>"/>
					</div>

					<div class="feature-repeater-item__description">
						<?php if ( $item->get_heading() ) : ?>
							<h3 class="feature-repeater-item__hd"><?php $item->heading(); ?></h3>
						<?php endif; ?>

						<div class="feature-repeater-item__content"><?php $item->content(); ?></div>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div><!-- .feature-repeater__items -->
</section><!-- .feature-repeater -->
